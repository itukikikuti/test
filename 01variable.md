# 変数

### 問1
``` C++
while (App::Refresh())
{
    camera.Update();
    sprite.position.x = 0.0f;
    sprite.position.y = 0.0f;
    sprite.Draw();
}
```
1. 画像が左に動く
2. 画像が上に動く
3. 画像は動かない
4. 画像が下に動く

### 問2
``` C++
while (App::Refresh())
{
    camera.Update();
    sprite.position.x += 1.0f;
    sprite.position.y = 0.0f;
    sprite.Draw();
}
```
1. 画像が左に動く
2. 画像が右に動く
3. 画像は動かない
4. 画像が下に動く

### 問3
``` C++
while (App::Refresh())
{
    camera.Update();
    sprite.position.x = 0.0f;
    sprite.position.y -= 1.0f;
    sprite.Draw();
}
```
1. 画像が下に動く
2. 画像は動かない
3. 画像が上に動く
4. 画像が右に動く

### 問4
``` C++
int x = 0.0f;
while (App::Refresh())
{
    camera.Update();
    x += 1.0f;
    sprite.position.x = x;
    sprite.position.y += 1.0f;
    sprite.Draw();
}
```
1. 画像が上に動く
2. 画像は動かない
3. 画像が左上に動く
4. 画像が右上に動く

### 問5
``` C++
int x = 0.0f;
int y = 0.0f;
while (App::Refresh())
{
    camera.Update();
    x += 1.0f;
    y -= 1.0f;
    sprite.position.x = x;
    sprite.position.y = y;
    sprite.Draw();
}
```
1. 画像が右上に動く
2. 画像が下に動く
3. 画像が右下に動く
4. 画像は動かない

### 問6
``` C++
int x = 0.0f;
int y = 0.0f;
while (App::Refresh())
{
    camera.Update();
    x -= 1.0f;
    y += 1.0f;
    sprite.position.x = x;
    sprite.position.y = y;
    sprite.Draw();
}
```
1. 画像が左下に動く
2. 画像が右上に動く
3. 画像が左上に動く
4. 画像が右下に動く

### 問7
``` C++
int x1 = 1.0f;
int x2 = -1.0f;
int y = 1.0f;
while (App::Refresh())
{
    camera.Update();
    y += 1.0f;
    sprite.position.x += x1 + x2;
    sprite.position.y = y;
    sprite.Draw();
}
```
1. 画像が上に動く
2. 画像が右上に動く
3. 画像は動かない
4. 画像が下に動く
