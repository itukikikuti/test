# 関数

### 問1
``` C++
while (App::Refresh())
{
    camera.Update();
    sprite.position.x = 0.0f;
    sprite.position.y = 0.0f;
    sprite.Draw();
}
```
1. 画像が左に動く
2. 画像が上に動く
3. 画像は動かない
4. 画像が下に動く
