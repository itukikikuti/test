# switch文

### 問1
``` C++
switch (0)
{
case 0:
    printf("0");
    break;
case 1:
    printf("1");
    break;
}
```
1. 0と出力される
2. 1と出力される

### 問2
``` C++
switch (2)
{
case 0:
    printf("0");
    break;
case 1:
    printf("1");
    break;
default:
    printf("else");
    break;
}
```
1. 0と出力される
2. 1と出力される
3. elseと出力される

### 問3
``` C++
switch (2)
{
case 0:
    printf("0");
    break;
case 1:
    printf("1");
    break;
case 2:
    printf("2");
    break;
default:
    printf("else");
    break;
}
```
1. 0と出力される
2. 1と出力される
3. 2と出力される
4. elseと出力される

### 問4
``` C++
int value = 4;
switch (value)
{
case 0:
    printf("0");
    break;
case 1:
    printf("1");
    break;
case 2:
    printf("2");
    break;
default:
    printf("else");
    break;
}
```
1. 0と出力される
2. 1と出力される
3. 2と出力される
4. elseと出力される

### 問5
``` C++
enum Mode
{
    Title,
    Game,
};
Mode mode = Title;
while (App::Refresh())
{
    switch (mode)
    {
    case Title:
        if (App::GetKey(VK_RETURN))
        {
            mode = Game;
        }
        printf("Title");
        break;
    case Game:
        printf("Game");
        break;
    }
}
```
1. Titleと出力される
2. Gameと出力される

### 問6
問5でエンターキーを押した場合はなにが出力されるでしょうか。
1. Titleと出力される
2. Gameと出力される
