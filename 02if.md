# if文

### 問1
``` C++
if (true)
{
    printf("true");
}
```
1. trueと出力される
2. falseと出力される

### 問2
``` C++
if (false)
{
    printf("true");
}
else
{
    printf("false");
}
```
1. trueと出力される
2. falseと出力される

### 問3
``` C++
if (!true)
{
    printf("true");
}
else
{
    printf("false");
}
```
1. trueと出力される
2. falseと出力される

### 問4
``` C++
bool flag = true;
if (flag)
{
    printf("true");
}
else
{
    printf("false");
}
```
1. trueと出力される
2. falseと出力される

### 問5
``` C++
int flag = 1;
if (flag == 0)
{
    printf("0");
}
else if (flag == 1)
{
    printf("1");
}
else
{
    printf("else");
}
```
1. 0と出力される
2. 1と出力される
3. elseと出力される

### 問6
``` C++
int value = 10;
if (value > 5)
{
    printf("true");
}
else
{
    printf("false");
}
```
1. trueと出力される
2. falseと出力される

### 問7
``` C++
float value = 0.1f;
if (value > 0.1f)
{
    printf("true");
}
else
{
    printf("false");
}
```
1. trueと出力される
2. falseと出力される
